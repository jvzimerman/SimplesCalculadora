package br.com.senac.simplecalculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtDisplay ;
    private EditText txtOperando1 ;
    private EditText txtOperando2 ;
    private Button btnSoma ;
    private Button btnSubtracao ;
    private Button btnMultiplicacao ;
    private Button btnDivisao ;

    private double operando1 ;
    private double operando2 ;
    private double resultado  = 0 ;


    private void atualizarDisplay(){
        txtDisplay.setText(String.valueOf(resultado));
    }


    private void calcular(String operacao){


        operando1 = Double.parseDouble(txtOperando1.getText().toString() ) ;
        operando2 = Double.parseDouble(txtOperando2.getText().toString() ) ;


        switch (operacao){
            case  "+"  : resultado = operando1 + operando2 ; break;
            case  "-"  :  resultado = operando1 - operando2 ; break;
            case  "*"  :  resultado = operando1 * operando2 ;break;
            case  "/"  :  resultado = operando1 / operando2 ; break;

        }

        this.atualizarDisplay();



    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtDisplay = findViewById(R.id.display);

        txtOperando1 = findViewById(R.id.operando1);
        txtOperando2 = findViewById(R.id.operando2);
        btnSoma = findViewById(R.id.btnSoma) ;
        btnSubtracao = findViewById(R.id.btnSubtracao);
        btnMultiplicacao = findViewById(R.id.btnMultiplicacao) ;
        btnDivisao = findViewById(R.id.btnDivisao);

        this.atualizarDisplay();



        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view ;
                calcular(btn.getText().toString());
            }
        });


        btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view ;
                calcular(btn.getText().toString());
            }
        });


        btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view ;
                calcular(btn.getText().toString());
            }
        });


        btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view ;
                calcular(btn.getText().toString());

            }
        });






    }
}
